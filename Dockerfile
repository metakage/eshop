FROM microsoft/dotnet:2.2-aspnetcore-runtime AS base
WORKDIR /eShopOnWeb
EXPOSE 80

FROM microsoft/dotnet:2.2-sdk AS build
WORKDIR /eShopOnWeb
COPY ["src/Web/Web.csproj", "src/Web/"]
RUN dotnet restore "src/Web/Web.csproj"
COPY . .
WORKDIR /eShopOnWeb
RUN dotnet build "/src/Web/Web.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "src/Web/Web.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "Web.dll","--environment:Development"]
